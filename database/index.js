///DB connect here
import Knex from "knex";

if (
  !process.env.DB_NAME ||
  !process.env.DB_HOST ||
  !process.env.DB_PASSWORD ||
  !process.env.DB_USER
) {
  throw Error("Please set database connection env variables");
}

export const knex = Knex({
  client: "mysql",
  connection: {
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    password: process.env.DB_PASSWORD,
    user: process.env.DB_USER,
  },
  pool: {
    afterCreate: function (conn, done) {
      conn.query("SET time_zone='Asia/Ulaanbaatar'", function (err) {
        done(err, conn);
      });
    },
  },
});
