"use client";

import useSWR from "swr";
import { useState } from "react";
import Box from "@mui/material/Box";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";


export default function Home() {
  const [interval, setInterval] = useState(7);

  const { data } = useSWR(
    interval == 7
      ? `/api/dashboard?interval=${interval}`
      : `/api/dashboard?interval=${interval}`
  );

  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));


  return (
    <Box sx={{ px: "32px", py: "24px", width: "inherit" }}>
      <Box
        sx={{
          border: "1px solid rgba(0, 0, 0, 0.2)",
          borderTopLeftRadius: "14px",
          borderTopRightRadius: "14px",
        }}
      >
        end General Dashbaord
        {console.log("DATA: ", data)}
      </Box>
    </Box>
  );
}
