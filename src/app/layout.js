"use client";

import { SWRConfig } from "swr";
import NextLink from "next/link";
import Box from "@mui/material/Box";
import { Login } from "./components";
import Navbar from "./components/NavBar";
import { Nunito } from "next/font/google";
import { useState, forwardRef } from "react";
import Typography from "@mui/material/Typography";
import CssBaseline from "@mui/material/CssBaseline";
import createTheme from "@mui/material/styles/createTheme";
import { UpdateLoading } from "./components/UpdateLoading";
import { SessionProvider, useSession } from "next-auth/react";
import ThemeProvider from "@mui/material/styles/ThemeProvider";

const nunito = Nunito({
  style: ["normal", "italic"],
  subsets: ["cyrillic", "cyrillic-ext", "latin", "latin-ext"],
  weight: "500",
});

// eslint-disable-next-line prefer-arrow-callback
const LinkBehaviour = forwardRef(function LinkBehaviour(props, ref) {
  return <NextLink ref={ref} {...props} />;
});




// Create a theme instance.
const theme = createTheme({
  palette: {
    primary: {
      main: '#007bff', // Blue from Material Design palette
    },
    secondary: {
      main: '#ffc107', // Amber from Material Design palette
    },
  },
  typography: {
    fontFamily: 'Roboto', // Set font family
  },
});


const fetcher = (...args) => fetch(...args).then((res) => res.json());

function LoginRequired({ children }) {
  const { status } = useSession();
  // eslint-disable-next-line no-nested-ternary
  return status === "loading" ? (
    <UpdateLoading />
  ) : status === "unauthenticated" ? (
    <Login />
  ) : (
    children
  );
}

export default function RootLayout({ children }) {
  const isSidebarOpen = true;
  const [isMobileSidebarOpen, setMobileSidebarOpen] = useState(false);
  return (
    <html>
      <head>
        <title>DBMS</title>
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link rel="icon" href="/favicon.png" />
      </head>
      <body style={{ height: "100vh" }} className={nunito.className}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <SWRConfig value={{ fetcher, revalidateOnFocus: false }}>
            <SessionProvider refetchOnWindowFocus refetchInterval={1800}>
              <LoginRequired>
                <>
                  <Navbar
                    isSidebarOpen={isSidebarOpen}
                    isMobileSidebarOpen={isMobileSidebarOpen}
                    onSidebarClose={() => setMobileSidebarOpen(false)}
                  />
                  <Box
                    sx={{
                      height: "inherit",
                      ml: { lg: "270px" },
                      pt: "66px",
                    }}
                  >
                    <Typography
                      sx={{
                        color: "#808080",
                        display: "flex",
                        fontSize: "14px",
                        justifyContent: "flex-end",
                        mr: "30px",
                        mt: "10px",
                      }}
                    ></Typography>

                    {children}
                  </Box>
                </>
              </LoginRequired>
            </SessionProvider>
          </SWRConfig>
        </ThemeProvider>
      </body>
    </html>
  );
}

