import Image from "next/image";
import Box from "@mui/material/Box";
import loaderLogo from "../../../public/loader.gif";

export function Loading() {
  return (
    <Box
      sx={{ alignItems: "center", display: "flex", justifyContent: "center" }}
    >
      <Image src={loaderLogo} alt="gif" height={150} />
    </Box>
  );
}

