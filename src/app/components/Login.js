import { useState } from "react";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";
import { signIn } from "next-auth/react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { useForm, Controller } from "react-hook-form";

export function Login() {
  const { control, handleSubmit } = useForm({
    defaultValues: { email: "", password: "" },
  });
  return (
    <Box
      sx={{
        alignItems: "center",
        backgroundAttachment: "fixed",
        backgroundPositionX: "center",
        backgroundPositionY: "bottom",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        display: "flex",
        flexDirection: "column",
        height: "inherit",
        pt: "148px",
        width: "inherit",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-evenly",
        }}
      >
        <Box
          sx={{
            borderRadius: "12px",
            flex: 0.4,
          }}
        >
          <Box sx={{ display: "flex", flex: 1, flexDirection: "row" }}>
            <Box
              sx={{
                backgroundColor: "#defaee",
                border: "1px solid rgba(0,0,0,.08)",
                borderRadius: "10px",
                boxShadow: "0 0 13px 0 rgba(8,83,141,.08)",
                display: "flex",
                flex: "0.5",
                flexDirection: "column",
                justifyContent: "center",
                position: "relative",
              }}
            >
              <form
                onSubmit={handleSubmit(async (credentials) => {
                  const signRes = await signIn("credentials", {
                    ...credentials,
                    redirect: false,
                  });
                })}
              >
                <Stack sx={{ p: "48px" }}>
                  <Controller
                    name="email"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <>
                        <Typography>Username</Typography>
                        <TextField
                          {...field}
                          sx={{ backgroundColor: "white", mt: "5px" }}
                        />
                        <Typography sx={{ fontSize: "12px" }}>
                          Enter your username
                        </Typography>
                      </>
                    )}
                  />
                  <Controller
                    name="password"
                    control={control}
                    rules={{ required: true }}
                    render={({ field }) => (
                      <>
                        <Typography sx={{ mt: "5px" }}>Password</Typography>
                        <TextField
                          type={"password"}
                          {...field}
                          sx={{ backgroundColor: "white", mt: "5px" }}
                        />
                        <Typography sx={{ fontSize: "12px" }}>
                          Enter your password
                        </Typography>
                      </>
                    )}
                  />
                  <Box
                    sx={{
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "space-evenly",
                    }}
                  >
                    <Box sx={{ display: "flex", justifyContent: "center" }}>
                      <Button type="submit" variant="contained">
                        Нэвтрэх
                      </Button>
                    </Box>
                    <Box>
                      <Typography
                        sx={{ fontSize: "14px", paddingY: "6px" }}
                        color="grey"
                        textAlign={"center"}
                      >
                      </Typography>
                    </Box>
                    <Button
                      onClick={() => signIn("google")}
                      variant="lightContained"
                    >
                      <Typography variant="span" sx={{ pl: "12px" }}>
                        Sign in with Google
                      </Typography>
                    </Button>
                  </Box>
                </Stack>
              </form>
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
