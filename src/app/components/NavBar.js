"use client";

import useSWR from "swr";
import Link from "next/link";
import * as React from "react";
import Image from "next/image";
import Box from "@mui/material/Box";
import List from "@mui/material/List";
import Drawer from "@mui/material/Drawer";
import Divider from "@mui/material/Divider";
import { useSession } from "next-auth/react";
import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import useMediaQuery from "@mui/material/useMediaQuery";
import ListItemButton from "@mui/material/ListItemButton";


const menu = {
  "/": {
    title: "Хянах самбар",
  },
  "/task": {
    module_name: "use_task",
    title: "Таскууд",
  },
  "/user": {
    module_name: "use_user",
    title: "Ажилчид",
  },
};

// permission бол хэрэглэгчийн эрхээс хамаарч тухайн menu харах боломжтой, үгүйг handle хийнэ. 
function MenuItem({
  link,
  title,
  index,
  module_name,
  onSidebarClose,
  ImageComponent,
  pl,
  permission,
}) {
  return  (
    <Link
      href={!module_name || permission[module_name] ? link : "#"}
      legacyBehavior
    >
      <a style={{ textDecoration: "none" }}>
        <ListItem disablePadding sx={{ height: "36px" }}>
          <ListItemButton
            sx={{
              height: "inherit",
              pb: "5px",
              pl: "32px",
              pr: "32px",
              pt: "5px",
              width: "100%",
            }}
            disabled={module_name ? !permission[module_name] : false}
          >
            <ListItemIcon
              style={{
                color: "#0054A6",
                fontSize: 18,
                minWidth: "32px",
                paddingLeft: pl != undefined ? pl : "0px",
              }}
            >
              {ImageComponent}
            </ListItemIcon>
            <ListItemText
              primaryTypographyProps={{ fontSize: "14px" }}
              onClick={onSidebarClose}
              primary={title}
              sx={{ color: "#434A51" }}
            />
          </ListItemButton>
        </ListItem>
      </a>
    </Link>
  );
}

function Item({ onSidebarClose }) {
  const { data, isValidating } = useSWR(() => `/api/user/permission`);

  const { data: sessionData } = useSession();
  const { user } = sessionData;

  return (
    <Box sx={{ bgcolor: "background.paper", height: "100%", maxWidth: 270 }}>
      <nav aria-label="main mailbox folders">
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            height: "65px",
            justifyContent: "start",
            paddingLeft: "32px",
            paddingRight: "32px",
          }}
        >
          <Image src={logo} alt="img" height={28} />
        </Box>
        <Divider />
        <List
          sx={{
            pb: "16px",
            pt: "16px",
          }}
        >
          {Object.entries( menu
          ).map(([link, value], index) => (
            <MenuItem
              key={link}
              link={link}
              {...value}
              index={index}
              onSidebarClose={onSidebarClose}
              permission={!isValidating ? data?.data : {}}
              is_admin={parseInt(user.is_admin) == 1}
              is_moderator={parseInt(user.is_moderator) == 1}
            />
          ))}
        </List>
      </nav>
    </Box>
  );
}

export default function BasicList({
  isSidebarOpen,
  isMobileSidebarOpen,
  onSidebarClose,
}) {
  const lgUp = useMediaQuery((theme) => theme.breakpoints.up("lg"));
  if (lgUp) {
    return (
      <Drawer
        anchor="left"
        open={isSidebarOpen}
        onClose={onSidebarClose}
        onClick={onSidebarClose}
        variant="persistent"
        PaperProps={{
          sx: {
            border: "0 !important",
            boxShadow: "0px 7px 30px 0px rgb(113 122 131 / 11%)",
            width: "270px",
          },
        }}
        elevation={16}
      >
        <Item onSidebarClose={onSidebarClose} />
      </Drawer>
    );
  }
  return (
    <Drawer
      anchor="left"
      open={isMobileSidebarOpen}
      onClose={onSidebarClose}
      onClick={onSidebarClose}
      variant="persistent"
      PaperProps={{
        sx: {
          border: "0 !important",
          boxShadow: "0px 7px 30px 0px rgb(113 122 131 / 11%)",
          width: "270px",
        },
      }}
      elevation={16}
    >
      <Item onSidebarClose={onSidebarClose} />
    </Drawer>
  );
}
