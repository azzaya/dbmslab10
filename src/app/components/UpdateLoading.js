import Image from "next/image";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
// import loaderLogo from "../../public/logo.png";

export function UpdateLoading() {
  return (
    <Box
      sx={{
        alignItems: "center",
        display: "flex",
        flexDirection: "column",
        height: "100%",
        justifyContent: "center",
      }}
    >
      <Typography component="h2" variant="bold" sx={{ pt: "40px" }}>
       Loading...
      </Typography>
    </Box>
  );
}
