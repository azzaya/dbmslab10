/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
// eslint-disable-next-line max-lines-per-function
exports.up = function (knex) {
    /**
     * @param { import("knex").Knex.CreateTableBuilder } table
     */
    function createdAt(table) {
      table.datetime("created_at").notNullable().defaultTo(knex.fn.now()).index();
    }
  
    return knex.schema.createTable("task_status", (table) => {
      table.increments("id");
      table.text("subject").notNullable();
      table.text("body").notNullable();
      table.integer("customer_id").notNullable();
      table.tinyint("duration_day").notNullable();
      table.tinyint("sort_order").notNullable();
      table.integer("price").notNullable();
      table.string("curreny", 10).notNullable();
      createdAt(table);
      table.datetime("updated_at").nullable();
    });
  };
  
  /**
   * @param { import("knex").Knex } knex
   * @returns { Promise<void> }
   */
  exports.down = function (knex) {
    return knex.schema.dropTable("task_status");
  };
  