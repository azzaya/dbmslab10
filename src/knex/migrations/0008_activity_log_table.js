/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
// eslint-disable-next-line max-lines-per-function
exports.up = function (knex) {
    /**
     * @param { import("knex").Knex.CreateTableBuilder } table
     */
    function createdAt(table) {
      table.datetime("created_at").notNullable().defaultTo(knex.fn.now()).index();
    }

    return knex.schema.createTable("activity_log", (table) => {
      table.increments("id");
      table.string("module", 255).notNullable();
      table.string("action", 255).notNullable();
      table.integer("body").notNullable();
      table.integer("user_id").notNullable();
      table.tinyint("task_id").notNullable();
      table.integer("save_data").notNullable();

      createdAt(table);
      table.datetime("updated_at").nullable();
    });
  };
  
  /**
   * @param { import("knex").Knex } knex
   * @returns { Promise<void> }
   */
  exports.down = function (knex) {
    return knex.schema.dropTable("activity_log");
  };
  