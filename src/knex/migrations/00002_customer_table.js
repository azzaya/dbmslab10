/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
// eslint-disable-next-line max-lines-per-function
exports.up = function (knex) {
    /**
     * @param { import("knex").Knex.CreateTableBuilder } table
     */
    function createdAt(table) {
      table.datetime("created_at").notNullable().defaultTo(knex.fn.now()).index();
    }
  
    return knex.schema.createTable("customer", (table) => {
      table.increments("id");
      table.string("firstname", 255).notNullable();
      table.string("lastname", 255).notNullable();
      table.integer("username").notNullable();
      table.integer("password").notNullable();
      table.tinyint("is_active").notNullable();
      table.integer("department_id").notNullable();
      createdAt(table);
      table.datetime("updated_at").nullable();
    });
  };
  
  /**
   * @param { import("knex").Knex } knex
   * @returns { Promise<void> }
   */
  exports.down = function (knex) {
    return knex.schema.dropTable("customer");
  };
  