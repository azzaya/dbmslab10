import { knex } from "database";
import { methodHandler } from "../../../utils/api";

export default methodHandler({
    get: async (req, res) => {
        try {
            // DB-ees shaardlagatai medeellee awaad, shaardlagatai handle hiigeed butsaah
            const resultData = await knex({ m: tableName })
                .where({ "m.company_id": company_id, "m.status_id": 1 })
                .leftJoin({ o: "task_status" }, "m.status_id", "o.id")
                .whereRaw(query)
                .select("o.name as name")
                .count({ countsum: "m.id" })
                .groupBy("o.name");
            res.status(200).json({
                data: resultData,
                reason: "Success",
            });
        } catch (e) {
            console.log(e);
            res.status(403).json({ reason: "Error" });
        }
    },
    post: async (req, res) => {
        try {
            // DB-ees shaardlagatai medeellee awaad, shaardlagatai handle hiigeed butsaah
            res.status(200).json({
                data: {"Test": "Test data"},
                reason: "Success",
            });
        } catch (e) {
            console.log(e);
            res.status(403).json({ reason: "Error" });
        }
    },
    put: async (req, res) => {
        try {
            // DB-ees shaardlagatai medeellee awaad, shaardlagatai handle hiigeed butsaah
            res.status(200).json({
                data: {"Test": "Test data"},
                reason: "Success",
            });
        } catch (e) {
            console.log(e);
            res.status(403).json({ reason: "Error" });
        }
    },
    delete: async (req, res) => {
        try {
            // DB-ees shaardlagatai medeellee awaad, shaardlagatai handle hiigeed butsaah
            res.status(200).json({
                data: {"Test": "Test data"},
                reason: "Success",
            });
        } catch (e) {
            console.log(e);
            res.status(403).json({ reason: "Error" });
        }
    },
});
