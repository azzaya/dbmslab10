import { knex } from "database";

export async function addActivityLog(
  module,
  action,
  body,
  user_id,
  task_id,
  is_admin_module,
  save_data = null
) {
  const log = await knex("activity_log").insert({
    action,
    body,
    task_id,
    created_at: knex.fn.now(),
    is_admin_module,
    module,
    save_data,
    user_id,
  });
  if (log) {
    return {
      countField: 1,
      status: "success",
    };
  }
  return {
    status: "error",
  };
}
