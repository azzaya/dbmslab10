import busboy from "busboy";

/**
 * @typedef { import("next").NextApiHandler } NextApiHandler
 * @typedef { 'delete'|'get'|'patch'|'post'|'put' } HTTPMethod
 * @param { {[key in HTTPMethod]: NextApiHandler} } handlers
 * @returns { NextApiHandler }
 */
export function methodHandler(handlers) {
  return (req, res) => {
    const handler = handlers[req.method.toLowerCase()];
    if (handler) handler(req, res);
    else res.status(405).end();
  };
}

/**
 * @param {import('next/server').NextRequest} req
 */
export function parseForm(req) {
  // eslint-disable-next-line no-undef
  return new Promise((resolve, reject) => {
    const parser = busboy({ headers: req.headers });
    const fields = {};
    const files = {};
    parser.on("file", (name, file, info) => {
      const data = [];
      file.on("data", (chunk) => {
        data.push(chunk);
      });
      file.on("end", () => {
        files[name] = { data: Buffer.concat(data), info };
        console.log(`parseForm: ${name} - ${files[name].data.length} bytes`);
      });
    });
    parser.on("error", (error) => reject(error));
    parser.on("close", () => {
      resolve({ fields, files });
    });
    req.pipe(parser);
  });
}
